# Songbookize

## Overview

`songbookize` is a ruby gem and command for generating songbooks in the same
style as [Zach's Songbook](http://zachcapalbo.com/projects/songbook.html). You
can refer to the [source code for Zach's Songbook](https://gitlab.com/zach-geek/songbook)
for reference.

## Installation

### Songbookize Gem

Installing the `songbookize` command is as simple as

```
gem install songbookize
```

This is enough to compile songbooks into HTML or YAML.

### Latex Dependencies

In order to generate the nice pdf songbooks and songbooklets, you'll need to
have LaTeX and a number of packages installed. If you're on Linux, the best
way to install everything is to install everything in the [Dockerfile](https://hub.docker.com/r/zachgeek/songbook-builder/~/dockerfile/).

On Windows, you can make it work with a LaTeX package like [MiKTeX](https://miktex.org/),
but it's tricky. Good luck.

### Docker Image

If installing all of the dependencies is too much work, there's a handy
[Docker Image](https://hub.docker.com/r/zachgeek/songbook-builder/) you can use
instead.

To use it, run something like:

```
docker run --rm -ti -v /path/to/your/songbook:/songbook zachgeek/songbook-builder
```

## Songbook Layout

Your songbook should be contained in its own folder, with all of the `.chords`
and `.abc` files living in the `Songs` subdirectory.

### Chords Format

The `.chords` format for songs is intended to be an easy, human readable way for
encoding song lyrics and chords.

#### Header

`.chords` files begin with a small yaml header:

```
---
title: A Song
key: A
desc: This is an example song
---
```

None of the header items are required, but some common ones are

- **title**: The song's title
- **key**: The musical key that the song is played in
- **desc**: A description of the song

#### Chords and Lyrics

After the header, chords are written with the chords on the line directly above
the lyrics that they correspond to. This is a format that has been commonplace
on the internet for decades. E.g.:

```
C         F          G
This is a song about nothing
C          F    G           Am
It doesn't mean anything at all
```

Lyrics don't have to have chords corresponding to them. Simply omit the chord
lines, and the verses will be picked up as being without chords. E.g.:

```
This is the second verse
But you know the chords from the first
```

You can denote that the a chorus by putting `[Chorus:]` before it. Then, to
refer to the chorus, you can just use `[Chorus]`

#### Example

So, our whole demo song from before might come out looking like:

```
---
title: A Song
key: A
desc: This is an example song
---

C         F          G
This is a song about nothing
C          F    G           Am
It doesn't mean anything at all

[Chorus:]
F           G
This is the chorus
D                C
It has different chords

This is the second verse
But you know the chords from the first

[Chorus]

And here's one more verse
Just for good measure, for good measure!
```

### ABC Format

[ABC Notation](http://abcnotation.com/) is a popular text format for inscribing
melodies. Songbookize will use standard tools to parse it, so you should refer
to [online references](http://abcnotation.com/) for more information on writing
ABC notation.

A song can have a `.abc` file, a `.chords`, file, or both. If both files are
specified for a given song, both the melody and the chords will be displayed.

### songbook.yml

`songbook.yml` describes the songbook. There are a few important keys:

- **title**: Sets the title of the song book
- **include**: Specifies a table of paths with a list of songs to copy.

For instance, you might have:

```
---
title: An Example Songbook
include:
  "/path/to/other/songbook/Songs":
    - a_song
    - a_nother_song
  "/path/to/other/otherSongbook/Songs":
   - a_nother_nother_song
```
