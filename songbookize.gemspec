VERSION = '0.0.5'
Gem::Specification.new do |s|
  s.name        = 'songbookize'
  s.version     = VERSION
  s.date        = Date.today
  s.summary     = 'Songbook Generator'
  s.description = 'Generate your own PDF/HTML songbook.'
  s.authors     = ["Zach Capalbo"]
  s.email       = "zach.geek@gmail.com"

  s.files       = %w[LICENSE Readme.md template.tex Rakefile Songbookize.md] + Dir["bin/*"] + Dir["Creator/*"]
  s.bindir      = "bin"
  s.executables = Dir["bin/*"].map{|f| File.basename(f)}
  s.homepage    = 'https://gitlab.com/zach-geek/songbook'
  s.license     = 'MIT'

  s.add_runtime_dependency "slim", "~> 3.0"
  s.add_runtime_dependency "sass", "~> 3.4.22"
  s.add_runtime_dependency "rake", "~> 10"
  s.add_runtime_dependency "json", "~> 1.8"
  s.add_runtime_dependency "kramdown", "~> 1"
  s.add_runtime_dependency "coffee-script", "~> 1"
end
