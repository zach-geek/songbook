# Zach's Songbook
_Songs for folks. Songs I like to play the way I like to play 'em_

I noticed that lot of times when people would come over to play music, there was a lot of time spent looking up chords and lyrics. And since it's folk music, most of the time the version on the internet would different from the version I know, which would cause a bit of confusion.

Thus, I decided to make my own songbook to keep around the house, for when people come over to play music and want to sing or play along with my versions of songs.

I'm not a real musician, and I never really play from song books or play exactly the same thing twice, so everything here is more of just a suggestion rather than what to play exactly. I also make no guarantee that any of my versions of songs will match anyone else's. If you want their version, get their songbook ☺

As far as I can tell, all of the songs in this book are either in the public domain, copylefted, or written by yours truly.

The latest version of this book can be found at <https://songbook.zachcapalbo.com>

The source code for this booklet can be found at <https://gitlab.com/zach-geek/songbook>

Instructions for making your own song book can be found at <https://gitlab.com/zach-geek/demo-songbook>

The songbook, the source code, and any songs here that I've written can be copied and shared under the Creative Commons Attribution-Share Alike license. <https://creativecommons.org/licenses/by-sa/2.5/>
