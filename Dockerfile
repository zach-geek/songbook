# Build typst with clear-to
FROM rust:1.70-buster AS typst
RUN cargo install --rev 6c542ea1a4a0ee85069dad6974ff331851eff406 --git https://github.com/typst/typst --root /usr

# FROM node:18 AS node_base 

# Pull base image.
FROM ubuntu:22.04
MAINTAINER Zach Capalbo <zach.geek@gmail.com>
RUN apt-get update
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y abcmidi ruby wget curl

RUN export LANG=en_US.UTF-8 && \
    export LANGUAGE=en_US.UTF-8 && \
    export LC_ALL=en_US.UTF-8

RUN apt install -y ruby-dev dos2unix build-essential libssl-dev

RUN gem install bundler

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -

RUN apt install -y nodejs


# RUN ln -s /usr/bin/nodejs /usr/bin/node

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY Fonts/*/*.ttf /usr/share/fonts/truetype

COPY --from=typst /usr/bin/typst /usr/bin/typst


ENV cachebust=2
# RUN gem install songbookize

COPY Gemfile /songbookize/Gemfile
COPY Gemfile.lock /songbookize/Gemfile.lock

RUN cd /songbookize && bundle install

COPY . /songbookize 
RUN find /songbookize -type f -print0 | xargs -0 dos2unix --

WORKDIR /songbook


#  Default command
CMD ["/songbookize/bin/songbookize"]
