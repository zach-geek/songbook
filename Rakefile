# encoding: UTF-8
require_relative 'Creator/parser.rb'
require_relative 'Creator/capo.rb'
# require_relative 'Creator/latex_formatter.rb'
require_relative 'Creator/typst_formatter.rb'
require_relative 'Creator/html_formatter.rb'
require_relative 'Creator/midi_formatter.rb'
require_relative 'Creator/bookletizer.rb'

def book
  unless $book
    $book = SongBook.parse("Songs")
    # $book.songs.each{|s| s.capo!(2) }
    # $book.preface = `tail -n+3 Readme.md | pandoc -t latex`
  end
  $book
end

def book_name
  book.file_safe_title
end

task :docker do
  system("docker build -t zachgeek/songbook-builder .")
end

task :indocker do
  system("docker run --rm -ti -v #{File.absolute_path(Dir.pwd).gsub("\\", "/")}:/songbook zachgeek/songbook-builder")
end

task :dockerpush do
  system("docker push zachgeek/songbook-builder")
end

task :parse do
  book
end

task :reorder => [:parse] do
  File.write("Songs/order.yml", Hash[book.songs.map{|s| [s.order, s.slug]}].to_yaml)
end

task :svg => [:parse] do
  if book.info.fetch("tunes", true) then
    mkdir_p("typst")
    Dir.chdir("typst") do
      book.songs.each(&:svg!)
    end
  end
end

desc "Install the local typst package"
task :typst_package do
  package_path = "#{ENV["LOCALAPPDATA"] || ENV["HOME"] + "/.cache"}/typst/packages/local/songbook-1.0.0"
  mkdir_p(package_path)
  cp(Dir[File.join(__dir__, "Creator", "typst", "**")], package_path)
end

desc "Builds PDF out of songbook."
task :book => [:parse, :reorder, :svg, :typst_package] do
  mkdir_p("typst")
  Dir.chdir("typst") do
    book.build(booklet:false, filename:"#{book_name}")    
  end
end

desc "Builds a booklet PDF out of songbook."
task :booklet => [:parse, :reorder, :svg, :typst_package] do
  mkdir_p("typst")
  Dir.chdir("typst") do
    book.build(booklet:true, filename:"#{book_name}let")
    Bookletizer.booklet2up("#{book_name}let.pdf", "#{book_name}2up.pdf", lastpage: false)
    Bookletizer.bookletize("#{book_name}let.pdf", "#{book_name}let.pdf", lastpage: true)
  end
end

desc "Builds book and booklet"
task :pdf => [:book, :booklet]

desc "Converts songbook into HTML"
task :html => [:parse, :reorder] do
  book.to_html("html")
end

desc "Creates midi files for all songs with .abc tunes"
task :midi => [:parse, :reorder] do
  book.to_midi("html/midi")
end

desc "Creates javascript / JSON descriptions of the songbook"
task :info => [:parse] do
  File.write("html/#{book_name}.json", book.json_info)
  File.write("html/#{book_name}.js", book.jsonp_info)
  File.write("html/songbook.json", book.json_info)
end

desc "Creates a portable single file archive of the songbook"
task :yaml => [:parse] do
  File.write("html/#{book_name}.yml", book.to_yaml)
end

desc "Attempts full build of all output types"
task :default => [:midi, :html, :info, :pdf] do
end

desc "Creates a tex file for each individual song"
task :songtex do
  b = SongBook.parse("Songs")
  Dir.chdir("latex") do
    b.songs.each do |song|
      File.write("#{song.title.downcase.gsub(/[^\w]/,"_")}.tex", SongBook.templatize(song.to_latex))
    end
  end
end

task :clean do
  Dir.chdir("Songs") do
    system("rm *.pdf *.ps *.eps *.mid")
  end
  system("rm latex/*")
end

task :toc_to_order do
  toc = File.read("latex/#{book_name}let.toc")
  nametonum = toc.each_line.map{|l| l.scan(/\\numberline \{(\d+)\}([^\}]+)\}/).flatten.reverse}
  File.write('Songs/order.yml', nametonum.map{|name, num| [num.to_i, b.songs.find{|s| s.title == name}.slug]}.to_yaml)
end

task :publish => [:preview, :booklet, :info] do
  raise StandardError.new("Uncommited changes") unless `git status -s`.empty?
  system("git push")
  system("scp -r latex/#{book_name}.pdf latex/#{book_name}let.pdf latex/#{book_name}.json #{book.info["remote_location"]}")
end
