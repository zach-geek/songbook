# encoding: UTF-8
require 'logger'
require 'yaml'
require 'open-uri'
require_relative 'song'

Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = Encoding::UTF_8

def log; $logger ||= Logger.new(STDOUT); return $logger; end

class Song
  def self.split_chords(line)
    return line.enum_for(:scan, /\b\w+\b/).map{[Regexp.last_match[0], Regexp.last_match.begin(0)]}
  end

  def parse_abc(f = nil)
    f ||= "#{File.dirname(file)}/#{slug}.abc"
    return log.debug("No such file #{f}") unless File.exist?(f)
    remove_sections = %w[T Q C N]
    self.melody = File.read(f)
    self.melody_file = File.absolute_path(f)
    self.file = File.absolute_path(f) if file.nil?
    self.info.merge!(self.melody.each_line.map{|l|l.split(":")}.reject{|i|i.size != 2}.inject({}){|h,k| h[k[0]]=k[1].strip; h})
    self.melody = self.melody.each_line.reject{|l| remove_sections.include?(l.split(":").first)}.join
    return self
  rescue
    log "Could not parse ABC for #{f}"
    raise
  end

  def parse_song_chords(f)
    state = :none
    last_key = :text
    current_line = Song::Line.new
    self.file = File.absolute_path(f)
    File.open(f).each_line do |raw_line|
      line = raw_line.strip
      # log.debug("#{state}: #{line[0..20]}")
      case (state)
      when :none then
        if line == "---" then
          log.debug "Starting header"
          state = :header
          next
        end

        if line.start_with?("Additional Verses") then
          next
        end

        unless line.empty? then
          log.debug "Starting new verse"
          state = :verse
          self.verses << Song::Verse.new
          redo
        end
      when :header then
        if line == "---" then
          log.debug "Ending header"
          state = :none
          next
        end

        key, value = line.scan(/(\w*):(.*)/).flatten
        log.debug("k: #{key}, v:#{value}, lk: #{last_key}")
        if key.nil? or value.nil? then
          info[last_key] += " " + line.strip
        elsif last_key
          info[key] = value.strip
          last_key = key
        end
      when :verse then
        if line.empty? then
          state = :none
          next
        end

        if line.match(/^\[?[Cc]horus:?\]?$/) then
          self.verses.last.chorus = true
          next
        end

        if raw_line.gsub('\r', '').match(/[ \t]{2}/) then
          current_line.raw_chords = raw_line.gsub('\t', '  ').gsub('\r', '').gsub('\n', '')
          current_line.chords = Song.split_chords(raw_line.gsub('\t', '  '))
          # log.debug "Chords #{current_line.chords}"
        else
          current_line.lyrics = line
          self.verses.last.lines << current_line
          current_line = Song::Line.new
        end
      end
    end

    return self
  end

  def self.parse(f)
    s = Song.new
    s.parse_song_chords(f)
    s.parse_abc
    return s
  end
end

class SongBook
  attr_reader :preface
  def parse(dir)
    if File.exist?("#{dir}/../songbook.yml") then
      log.debug "Reading info file"
      @info = Hash[YAML.load(File.read("#{dir}/../songbook.yml")).map{|(k,v)| [k.to_sym,v]}]
    else
      raise StandardError.new("No such file #{dir}/songbook.yml")
    end

    Dir["#{dir}/*.chords"].each do |f|
      songs << Song.parse(f)
    end

    Dir["#{dir}/*.abc"].reject{|a| File.exist?("#{dir}/#{File.basename(a, ".abc")}.chords")}.each do |f|
      songs << Song.new.parse_abc(f)
    end

    # Allow include: syntax in songbook.yml to include songs from other songbooks
    @info.fetch(:include, {}).each do |inc_dir, song_list|
      Dir["#{inc_dir}/*.chords"].each do |f|
        new_song = Song.parse(f)
        songs << new_song if song_list.nil? or song_list.empty? or song_list.include?(new_song.slug)
      end
      Dir["#{inc_dir}/*.abc"].reject{|a| File.exist?("#{inc_dir}/#{File.basename(a, ".abc")}.chords")}.each do |f|
        new_song = Song.new.parse_abc(f)
        songs << new_song if song_list.nil? or song_list.empty? or song_list.include?(new_song.slug)
      end
    end

    if File.exist?("#{dir}/order.yml") then
      # Looks better in the yml file as ##: slug
      order = Hash[YAML.load(File.read("#{dir}/order.yml")).map{|k,v| [v, k]}] || {}
      max = order.values.max || 0
      songs.each{|s| s.order = order.fetch(s.slug, max += 1) || max += 1}
      songs.sort_by!(&:order)
      songs.each_with_index{|s, i| s.order = i + 1}
    end

    if File.exist?("#{dir}/../Readme.md") then
      @preface = File.read("#{dir}/../Readme.md")
    else
      @preface = @info.fetch(:preface, "")
    end

    return self
  end

  def self.parse(dir)
    return SongBook.new.parse(dir)
  end
end
