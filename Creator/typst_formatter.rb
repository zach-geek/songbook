#encoding: utf-8
require 'open3'
require 'fileutils'
require 'logger'
require 'shellwords'
require 'rqrcode'

require_relative 'song.rb'
require_relative 'bookletizer.rb'

def log; $logger ||= Logger.new(STDOUT); return $logger; end

class Song
    class Line
        def to_typst
            return "#verseline[#{lyrics.gsub("'", "’")}]" if chords.nil? or chords.empty?
            offset = 0
            res = lyrics.dup
            chords.each do |chord, pos|
              cmd = %|#chord(\"#{chord.gsub(/[\/#]/, "\\$0")}\")|
              res = res.ljust(pos + offset + 1, "\001").insert(pos + offset, cmd)
              offset += cmd.length
            end
      
            return "#verseline(chords: true)[" + res.gsub("'", "’").gsub(/\001+/){|r| "#hide[#{"m" * r.length}]"} + "]"
        end
    end

    class Verse
        def to_typst
          lstr = lines.collect{|l| "#{l.to_typst} \\ "}.join("\n") + "\n"
          if lstr.strip.empty? then
            return "#chorus-here" if chorus?
            return ""
          end
          lstr = "#chorus\n" + lstr if chorus?
          return "#verse[\n" + lstr + "\n]\n"
        end
      end

    def to_typst(options = {})
        return "" if verses.empty? and (melody.nil? or melody.empty? or not options.fetch(:tunes, true))
        parts = []
        parts << "#song("
        parts << "title: [#{title}],"
        parts << "author: [#{author}],"
        parts << "description: [#{desc}],"
        parts << "short: #{info.fetch("short", false) or verses.empty?},"
        parts << "rise-up: [#{info["rise_up"]}]," if info["rise_up"]
        parts << "rise-again: [#{info["rise_again"]}]," if info["rise_again"]
        parts << "key: [#{info["key"]}]," if info["key"]
        parts << "melody-img: image(\"./#{slug}.svg\")," unless melody.nil? or melody.empty? or not options.fetch(:tunes, true)
        parts << %<qrlink: image("#{slug}-link.svg"),> if options[:web_url]
        parts << ")["


        unless verses.empty? then
          parts << verses.collect{|v| v.to_typst}.join("\n")
        end

        parts << "]"

        return parts.join("\n")
    end

    def svg_melody!
      return nil if melody.nil? or melody.empty?

      converter = File.absolute_path(File.join(__dir__, "../SvgConverter/convert.js"))

      return nil if FileUtils.uptodate?("#{slug}.svg", [
        self.melody_file,
        converter,
        # __FILE__
      ])

      log.debug "Converting #{self.melody_file}"
      
      Open3.popen2("node", converter) do |i,o,t|
        other_info = []
        # other_info << "%%textfont Times-Italic 14"
        # other_info << "%%text Key of #{self.key}"
        # other_info << "%%textoption right"
        # other_info << "%%text #{self.author}"
        i.print(melody.sub(/^([^:]*)$/, "#{other_info.join("\n")}\\1").to_s)
        i.close
        svg = o.read
        t.join
        File.write("#{slug}.svg", svg)
      end
    end

    def svg!
      svg_melody!
      
      File.write("#{slug}-link.svg", RQRCode::QRCode.new("https://zach-geek.gitlab.io/songbook/#{slug}.html").as_svg)
    end
end

class SongBook
    def to_typst(options = {})
        template_path = File.absolute_path(File.join(File.dirname(__FILE__), "../Creator/songbooklet.typ"))
        template_path = "/" + template_path if template_path.start_with?("/")

        typst_preface = options[:preface].to_s.gsub(/<.*?>/, "#link(\"\\0\")").gsub(/^#+\s/){|r| r.gsub("#", "=")}

        lines = []
        lines << "#import \"@local/songbook:1.0.0\": *"
        lines << "#show: songbooklet.with(booklet: #{options.fetch(:booklet, true)})"
        lines << ""
        lines << "#songbooklet-title-page(title: [#{options.fetch(:title, "Untitled Songbook")}], subtitle: [#{options.fetch(:subtitle, "")}], date: \"#{Date.today.to_s}\")[#{typst_preface}]"
        lines += songs.reject{|s| s.hidden?}.sort_by do |s|
            #[s.song? ? 0 : 1, s.short? ? 1 :0, File.ctime(s.file)]
            s.order
          end.collect{|s| s.to_typst(options)}
          return lines.join("\n")
    end
    def build(options)
        options.merge!(@info)
        filename = options.fetch(:filename, "preview")
        File.write("#{filename}.typ", to_typst(options.merge(preface:self.preface)))
        system("typst --font-path #{Shellwords.escape(File.absolute_path(File.join(__dir__, "../Fonts")))} c #{filename}.typ") or raise StandardError.new("Could not compile typst document")
    end
end