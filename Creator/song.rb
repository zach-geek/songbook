#encoding: utf-8
class Song
  attr_accessor :info, :file, :verses, :melody, :order, :melody_file
  def initialize
    @verses = []
    @info = {}
  end

  def title; (info["title"] || info["T"]).to_s.gsub("'", "’"); end
  def desc; (info["desc"] || info["N"]).to_s.gsub("'", "’"); end
  def key; (info["key"] || info["K"]).to_s.gsub("maj", "").gsub("min", "m"); end
  def author; info["author"] || info["composer"] || info["C"] || "Traditional"; end
  def emoji; info.fetch("emoji", "").strip.split(""); end

  def slug; File.basename(file, File.extname(file)); end

  def song?; return !tune?; end
  def tune?; return verses.empty?; end
  def hidden?; info['skip'] == "true"; end
  def short?; info['short'] == 'true'; end
end

class Song
  class Verse
    attr_accessor :lines, :chords, :chorus

    def initialize
      @lines = []
    end
    def chorus?; @chorus; end
    def chorus_indicator?; chorus? and lines.empty?; end
  end

  class Line
    attr_accessor :lyrics, :chords, :raw_chords
  end

  class Melody
  end
end

class SongBook
  attr_accessor :songs, :preface, :info
  def initialize
    @songs = []
  end

  def title; info[:title]; end
  def subtitle; info[:subtitle]; end

  def file_safe_title; title.gsub(/[^\w]/,""); end
end
