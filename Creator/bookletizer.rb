require 'hexapdf'
require 'ostruct'

# based on https://gitlab.com/wieke/bookletify/-/blob/master/src/worker.js
module Bookletizer
    def self.order_pages(pages, options = {})
        n = pages.size
        return OpenStruct.new(forward: pages.dup, backward: []) if n == 1

        if n == 2 and options[:lastpage] then
            return OpenStruct.new(forward: [pages[0]], backward: [pages[1]])
        end

        deficit = (4 - n % 4) % 4
        reverse_from = deficit == 0 ? n / 2 : ((n + deficit) / 2).ceil

        forward = pages.dup
        backward = forward.slice!(reverse_from, n).reverse
        1.upto(forward.length - backward.length) do |i|
            if options[:lastpage] then
                backward.insert(1, nil)
            else
                backward.unshift(nil)
            end
        end
        return OpenStruct.new(forward: forward, backward: backward)
    end

    def self.bookletize(path, output_path, options = {})
        doc = HexaPDF::Document.open(path)
        original_pages = doc.pages.to_a

        center = original_pages[0][:MediaBox].width
        width = original_pages[0][:MediaBox].width * 2
        height = original_pages[0][:MediaBox].height

        ordered_pages = self.order_pages(original_pages, options)

        ordered_pages.forward.each_with_index do |forward_page, i|
            page = doc.pages.add()
            page[:MediaBox].width = width
            page[:MediaBox].height = height

            switched = (i % 2) == 0
            at = switched ? [center, 0] : [0, 0]
            page.canvas.xobject(forward_page.to_form_xobject, at: at)

            backward_page = ordered_pages.backward[i]
            next if backward_page.nil?
            at = switched ? [0, 0] : [center, 0]
            page.canvas.xobject(backward_page.to_form_xobject, at: at)
        end

        original_pages.each {|page| doc.pages.delete(page)}

        doc.write(output_path)
    end

    def self.booklet2up(path, output_path, options = {})
        doc = HexaPDF::Document.open(path)
        original_pages = doc.pages.to_a

        center = original_pages[0][:MediaBox].width
        width = original_pages[0][:MediaBox].width * 2
        height = original_pages[0][:MediaBox].height

        page = doc.pages.add()
        page[:MediaBox].width = width
        page[:MediaBox].height = height
        page.canvas.xobject(original_pages[0].to_form_xobject, at: [center / 2, 0])

        original_pages[1..-1].each_slice(2) do |forward_page, backward_page|
            page = doc.pages.add()
            page[:MediaBox].width = width
            page[:MediaBox].height = height

            switched = false #(i % 2) == 0
            at = switched ? [center, 0] : [0, 0]
            page.canvas.xobject(forward_page.to_form_xobject, at: at)

            next if backward_page.nil?
            at = switched ? [0, 0] : [center, 0]
            page.canvas.xobject(backward_page.to_form_xobject, at: at)
        end

        original_pages.each {|page| doc.pages.delete(page)}

        doc.write(output_path)
    end
end