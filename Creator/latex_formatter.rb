#encoding: utf-8
require 'open3'
require_relative 'song.rb'

class Song
  class Line
    def to_latex
      return lyrics.gsub("'", "’") if chords.nil? or chords.empty?
      offset = 0
      res = lyrics.dup
      chords.each do |chord, pos|
        cmd = %|\\ch{#{chord}}|
        res = res.ljust(pos + offset + 1, " ").insert(pos + offset, cmd) # Note, this is a unicode emspace
        offset += cmd.length
      end

      return res.gsub("'", "’")
    end
  end

  class Verse
    def to_latex
      lstr = lines.collect{|l| "#{l.to_latex} \\\\\\*"}.join("\n")
      return "" if lstr.strip.empty?
      lstr = "\\chorus\n" + lstr if chorus?
      template =
"\\begin{songverse}
#{lstr}
\\end{songverse}"
      return template
    end
  end

  def to_latex
    parts = []
    case
    when short? then parts << "\\newshortsong"
    when song? then parts << "\\newsong"
    when tune? then parts << "\\newtune"
    end
    parts << "\\section{#{title}}"
    parts << "\\songdesc{#{desc}}\n\n" if desc
    parts << "\\#{(melody.nil? or melody.empty?) ? "nm" : ""}songinfo{#{key.empty? ? "No Key Specified" : "Key of #{key}"}}{#{author}}"
    parts << "\\melody{#{slug}001.eps}" unless melody.nil? or melody.empty?
    # parts << "\\begin{abc}[name=#{title.downcase.gsub(/[^\w]/, "")}]\n#{melody}\\end{abc}" unless melody.nil? or melody.empty?
    # parts << "\\begin{lilypond}[quoted,staffsize=26]\n#{lilypond}\\end{lilypond}" unless melody.nil? or melody.empty?

    unless verses.empty? then
      parts << "\\begin{lyrics}"
      parts << verses.collect{|v| v.to_latex}.join("\n")
      parts << "\\end{lyrics}"
    end

    return parts.join("\n")
  end

  def eps!
    return nil if melody.nil? or melody.empty?
    Open3.popen2("abcm2ps -E -O #{slug} -") do |i,o,t|
      other_info = []
      # other_info << "%%textfont Times-Italic 14"
      # other_info << "%%text Key of #{self.key}"
      # other_info << "%%textoption right"
      # other_info << "%%text #{self.author}"
      i.print(melody.sub(/^([^:]*)$/, "#{other_info.join("\n")}\\1").to_s)
      i.close
      t.join
    end
    return "#{slug}001.eps"
  end

  def preview_latex
    Dir.chdir("/tmp/") { eps! }
    File.write("/tmp/preview.tex", SongBook.templatize(to_latex))

    Dir.chdir("/tmp") do
      return unless system("xelatex -shell-escape /tmp/preview.tex")
    end
    spawn("evince /tmp/preview.pdf")
  end
end

class SongBook
  def to_latex(options = {})
    SongBook.templatize(songs.reject{|s| s.hidden?}.sort_by do |s|
      #[s.song? ? 0 : 1, s.short? ? 1 :0, File.ctime(s.file)]
      s.order
    end.collect{|s| s.to_latex}.join("\n"), options.merge(@info))
  end

  def build(options = {})
    filename = options.fetch(:filename, "preview")
    File.write("#{filename}.tex", to_latex(options.merge(preface:self.preface)))
    2.times { raise StandardError.new("Could not build #{filename}") unless system("xelatex -shell-escape #{filename}.tex") }
  end

  def preview(options = {})
    build(options)
    spawn("evince #{filename}.pdf")
  end

  def self.templatize(str, options={})
    template = File.read(options[:template] || "#{File.dirname(__FILE__)}/../template.tex")
    if options[:booklet]
      template.gsub!('documentclass[letterpaper]{article}', 'documentclass[letterpaper]{book}')
      template.gsub!('usepackage[noprint]{booklet}', 'usepackage[print]{booklet}')
    end
    template.sub("SONGGOESHERE", str)
            .sub("PREFACEGOESHERE", options[:preface].to_s)
            .sub("TITLEGOESHERE", options.fetch(:title, "Untitled Songbook"))
            .sub("SUBTITLEGOESHERE", options.fetch(:subtitle, ""))
  end
end
