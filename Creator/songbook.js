var OCTAVE;

OCTAVE = "A Bb B C Db D Eb E F Gb G Ab".split(" ");

window.capo = function(chord, fret) {
  var idx, start;
  idx = OCTAVE.indexOf(chord.slice(0, 2));
  start = 2;
  if (idx < 0) {
    idx = OCTAVE.indexOf(chord[0]);
    start = 1;
  }
  if (idx < 0) {
    throw new Exception(`Unknown chord ${chord}`);
  }
  return `${OCTAVE[(idx + fret) % OCTAVE.length]}${chord.slice(start)}`;
};

window.transpose = function(steps) {
  return d3.selectAll(".chords").each(function(c) {
    return $(this).text($(this).text().replace(/[^\s]+/g, function(t) {
      return capo(t, steps);
    }));
  });
};

window.currentTranspose = 0;

window.transposeUp = function() {
  window.currentTranspose += 1;
  return window.transpose(1);
};

window.transposeDown = function() {
  window.currentTranspose -= 1;
  return window.transpose(-1);
};
