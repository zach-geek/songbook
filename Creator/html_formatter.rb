# encoding: UTF-8
require 'json'
require 'date'
require 'slim'
require 'ostruct'
require 'kramdown'
require_relative 'song'

class Song
  class Line
    def to_html
      return lyrics.gsub("'", "’").strip.each_line.map{|l| %|<span class="lyricline">#{l}</span>|}.join("\n") if chords.nil? or chords.empty?
      return %|<span class="chords">#{raw_chords}</span><span class="chordlyrics">#{lyrics.gsub("'", "’").strip}</span>|

      return res.gsub("'", "’")
    end
  end

  class Verse
    def to_html
      lstr = lines.map(&:to_html).join("\n")
      if chorus_indicator?
        lstr = "<div class='chorusindicator'>[Chorus]</div>"
      elsif chorus?
        lstr = "<span class='chorusindicator'>Chorus:</span>\n<div class='chorus'>#{lstr}</div>"
      end

      return lstr
    end
  end

  class Melody
    def to_html
      sub(/^([^:]*)$/, "#{other_info.join("\n")}\\1").to_s
    end
  end

  def to_html
    verses.map(&:to_html).join("\n\n")
  end
end

class SongBook
  def json_info
    info = {}
    info['songs'] = songs.reject(&:hidden?).map(&:title)
    info['songInfo'] = songs.reject(&:hidden?).map do |song|
      Hash[%w[title slug key author desc emoji].map{|f| [f, song.__send__(f.to_sym)]}]
    end
    info['date'] = DateTime.now
    info['date_format'] = DateTime.now.strftime("%B %-d, %Y")

    return info.to_json
  end

  def jsonp_info
    return %|window.songBookInfo = #{json_info};|
  end

  def to_html(dir, opts = {})
    scope = OpenStruct.new(book: self)
    log.info "Creating songbook in #{dir}"
    File.write("#{dir}/songbook.html", Slim::Template.new(File.join(File.dirname(__FILE__),'songbook.slim')).render(scope))
    File.write("#{dir}/tv.html", Slim::Template.new(File.join(File.dirname(__FILE__),'tv.slim')).render(scope))
    log.debug "Options: #{opts}"
    songs.each do |song|
      next if opts.has_key?(:only) and not opts[:only].include?(song.slug)
      log.debug("Creating song page for #{song.title}")
      scope = OpenStruct.new(song: song)
      File.write("#{dir}/#{song.slug}.html", Slim::Template.new(File.join(File.dirname(__FILE__),'song.slim')).render(scope))
      # File.write("#{dir}/#{song.slug}_slideable.html", Slim::Template.new(File.join(File.dirname(__FILE__),'slideable_song.slim')).render(scope))
    end
    FileUtils.cp Dir["#{File.dirname(__FILE__)}/*.js"], "#{dir}/"
    FileUtils.cp Dir["#{File.dirname(__FILE__)}/resources/*"], "#{dir}/"
  end
end
