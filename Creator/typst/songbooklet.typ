#let secondary-font = ("Source Sans 3", "Source Sans Pro")
#let is-booklet = state("booklet", true)

#let chord(c) = {
    box(place(dy: -18pt, text(weight: "regular", font: secondary-font, c)))
}

#let desc(d) = {
    box(width: 100%, stroke: (left: 1pt + gray, top: 1pt + gray), pad(12pt, text(size: 8pt, d)))
}

#let verseline(chords: false, content) = {
    if chords { v(0.75em) }
    par(hanging-indent: 1em, content)
}

#let chorus = {
    text(weight: "bold", font: secondary-font, [\[Chorus:\]#linebreak()])
    v(0.5em)
}

#let chorus-here = {
    // h(1em)
    text(weight: "light", size: 0.8em,font: secondary-font, [\[Chorus\]#linebreak()])
    // v(0.5em)
}

#let verse(v) = {
    show par: set block(spacing: 0.1em)
    block(breakable: false, above: 2.2em, v)
}

#let melody(img, body-height) = {
    locate(loc => {
        if is-booklet.at(loc) {
            return img
        }

        if body-height < 2in {
            return img
        }

        let max-melody-height = 3in

        if body-height > 800pt {
            max-melody-height = 2in
        }        

        style(sty => {
            let m = measure(img, sty)
            if m.height > max-melody-height {
                align(center, block(height: max-melody-height, img))
            } else {
                img
            }
        })
    })
}

#let layout-song-body(loc, body) = {
    if is-booklet.at(loc) {
        body
    } else {
        v(2em)
        columns(2, gutter:3em, body)
    }
}

#let song(title: none, 
          author: none,
          description: none,
          rise-up: none,
          rise-again: none,
          key: none,
          melody-img: none,
          qrlink: none,
          short: false,
          body) = {
    locate(loc => {
        style(sty => {
            if short or not is-booklet.at(loc) {
                pagebreak(weak: false)
            } else {
                pagebreak(weak: false, to: "even")
            }
            heading(title)
            if qrlink != none { 
                place(top + right, dy: -0.3in, block(width: 0.4in, height: 0.4in, qrlink) )
            }
            let cells = (
                    if author != none { par[Author: #author] },
                    if key != none { par[Key: #key]},
                    if rise-up != none { par[#smallcaps[Rise Up] page #rise-up] },
                    if rise-again != none { par[#smallcaps[Rise Again] page #rise-again] },
            ).filter(i => i != none);
            desc({
                {
                    table(columns: (1fr,) * (cells.len()),
                    stroke: none,
                    inset: 0pt,
                    align: (c,r) => if c == 0 { left } else if c == 2 or c == (cells.len() - 1) { right } else { center },
                    ..cells
                    )
                }

                par(justify: true, description)
            })

            let body-size = measure(layout-song-body(loc, body), sty)

            if melody-img != none {
                melody(melody-img, body-size.height)
            }

            layout-song-body(loc, body)
        })
    })
}

#let songbooklet(booklet: true, body) = {    
    let height = 11in
    let width = 8.5in
    let margin = 0.5in
    if booklet {
        height = 8.5in
        width = 5.5in
        margin = auto
    }
    set page(height: height, width: width, margin: margin)

    set text(font: ("Junge", "Noto Emoji"), size: 10pt)
    set heading(numbering: "1. ")
    set page(background: {
        is-booklet.update(booklet)
        place(left + top, line(stroke: 2pt, length: 100%, angle: 90deg))
        place(right + top, line(stroke: 2pt, length: 100%, angle: 90deg))
    })
    body

    pagebreak()
    place(center + horizon, text(size: 32pt, font: "Noto Emoji", stack(dir: ltr, [🪕], rotate(-90deg, "🎻"))))
    place(center + bottom, [
        #image("./songbooklink1.svg", height: 1in)
        This songbook has been created with Zach's Songbook Creator.

        https://zachcapalbo.com/projects/songbook.html

        You too can create your own songbook!
        
    ])
}

#let songbooklet-title-page(title: "Untitled Songbook", subtitle: "", date: none, preface) = {
    place(center + horizon,
        align(center, {
            block(text(size: 32pt, title))
            v(15pt)
            block(width: 50%, par(emph(subtitle)))
        }))
    if date != none {
        locate(loc => {
            place(center + bottom, [As of #date #linebreak() #counter(heading).final(loc).first() songs])
        })
    }
    pagebreak()
    {
        set par(justify: true)
        set heading(numbering: none, outlined: false)
        preface
    }
    pagebreak(weak: true)
    locate(loc => {
        text(size: 16pt, [Contents])
        v(1em)
        let elems = query(heading.where(outlined: true), loc)
        for hh in elems {
            block[
                #set text(size: 11pt)
                #counter(heading).at(hh.location()).first(). #h(0.5em) #hh.at("body")
            ]
        }
    })
}

#show: songbooklet.with(booklet: false)

#songbooklet-title-page(title: "Testa Typst", [
    Test of typst layout and everything 🪕
])

#style(sty => measure([], sty))
#song(
author: [Zach Capalbo],
description: [I wrote this song early into my senior year of college, when I was rather unsatisfied with my lot in life—overworked and underpaid.  I obviously drew from Woody Guthry’s "I Ain’t Got No Home", which I wanted to sing, but felt it was too political to express what I wanted, which at the time I felt was more an existential calamity. I also drew on classic hobo ballads, dreaming of just up and wandering away from all my studies and responsibilities.],
short: false,
// melody-img: image("../typst/aint_got_no_home.svg"),
title: [Ain’t Got No Home In This World])[
#verse[
#verseline(chords: true)[#chord("C")My shoes is all torn up my #chord("F")toes is stickin’ out,] \
#verseline(chords: true)[If I #chord("C")don’t get some whiskey, gonna #chord("Gsus2")go up the spout.] \
#verseline(chords: true)[#chord("C")I ain’t got no jacket, #chord("F")I ain’t got no tie,] \
#verseline(chords: true)[And I #chord("C")ain’t got no #chord("G")home in this #chord("C")world.] \

]

#verse[
#chorus
#verseline(chords: true)[I #chord("C")ain’t got no home in this world] \
#verseline(chords: true)[I #chord("F")ain’t got no home in this #chord("Gsus2")world] \
#verseline(chords: true)[My #chord("C")money is gone and my #chord("F")toes is cold] \
#verseline(chords: true)[And I #chord("C")ain’t got no #chord("G")home in this #chord("C")world.] \

]

#verse[
#verseline[My friends have all gone and I can’t get them back.] \
#verseline[My company now’s just a cigarette pack.] \
#verseline[But I ain’t got a light, I just got a dark,] \
#verseline[And I ain’t got no home in this world.] \

]

#chorus-here
#verse[
#verseline[I ain’t got no sweethearts; I ain’t got no sweets.] \
#verseline[I think my poor heart has been missin’ some beats.] \
#verseline[I’ll dream up some love, ’neath an old willow tree] \
#verseline[’Cause I ain’t got no home in this world.] \

]

#chorus-here
#verse[
#verseline[But I am happy, I ramble and roam] \
#verseline[And them that don’t like me they leave me alone.] \
#verseline[I’ll pluck on my banjo; I’ll sing and I’ll shout] \
#verseline[That I ain’t got no home in this world.] \

]

#chorus-here
#verse[
#verseline[When I’m in the graveyard, and I’m laid to rest] \
#verseline[And everyone there thinks I’m happy and blessed] \
#verseline[Won’t have no more money, won’t have no banjo—] \
#verseline[But I’ll have a home in this world!] \

]

#chorus-here
]