class Song
  def to_midi
    return nil if melody.nil? or melody.empty?
    Open3.popen2("abc2midi - -o #{slug}.mid") do |i,o,t|
      other_info = []
      # other_info << "%%textfont Times-Italic 14"
      # other_info << "%%text Key of #{self.key}"
      # other_info << "%%textoption right"
      # other_info << "%%text #{self.author}"
      i.print(melody.sub(/^([^:]*)$/, "#{other_info.join("\n")}\\1").to_s)
      i.close
      t.join
    end
    return "#{slug}.mid"
  end
end

class SongBook
  def to_midi(dir)
    FileUtils.mkdir_p dir
    Dir.chdir(dir) do
      songs.each(&:to_midi)
    end
  end
end
