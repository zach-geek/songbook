class Song
  def capo!(fret)
    capo_melody!(fret) unless melody.nil? or melody.empty?
    verses.each{|v| v.capo!(fret)}
    %w[key K].each{|k| info[k] = Song.capo(info[k], fret) if info.include?(k)}
    return self
  end

  OCTAVE ||= %w[A Bb B C Db D Eb E F Gb G Ab]
  def self.capo(chord, fret)
    idx = OCTAVE.index(chord[0..1])
    range = 0..1
    if idx.nil? then
      idx =  OCTAVE.index(chord[0])
      range = 0
    end
    raise StandardError.new("Unknown Chords #{chord}!") if idx.nil?

    chord[range] = OCTAVE[(idx + fret) % OCTAVE.size]
    return chord
  end

  private
  def capo_melody!(fret)
    Open3.popen2("abc2abc - -t #{fret}") do |i,o,t|
      i.print(melody.to_s)
      i.close
      t.join
      self.melody = o.read
    end
  end
end

class Song
  class Verse
    def capo!(fret)
      lines.each{|l| l.capo!(fret)}
    end
  end

  class Line
    def capo!(fret)
      log.debug self
      log.debug chords
      return if chords.nil? or chords.empty?
      chords.each do |chord, pos|
        Song.capo(chord, fret)
      end
      log.debug self
      log.debug chords
    end
  end
end
