const {JSDOM} = require ('jsdom');
const fs = require('fs');

process.stdin.setEncoding('utf-8');
process.stdin.resume();

(async() => {
  let content = []
  await new Promise((r, e) => {
    process.stdin.on('data', function(buf) { 
      content.push(buf);
    });
    process.stdin.on('end', r)
  });


  let dom = new JSDOM(`<div id="abc"></div>`)
  let {window} = dom
  let {document, navigator} = window
  globalThis.document = document
  globalThis.window = window
  globalThis.navigator = navigator
  let abc = require('./abcjs.js')
  abc.renderAbc("abc", content.join())

  document.querySelector("#abc > svg").setAttribute('xmlns', "http://www.w3.org/2000/svg")
  
  console.log(document.getElementById("abc").innerHTML)
  process.stdout.end()
})();
